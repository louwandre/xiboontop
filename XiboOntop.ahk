

;
; XiboWD Version: 		0.4
; Language:       		AutoHotkey
; Platform:       		Windows7/8.1
; Author:         		Andre Louw <alouw@apcglobal.com>
;
; Script Function:
;	- This scripts Monitors a Xibo Player
;	- Once started the script will determine if the machine it is running on is 32bit or 64bit and set configuration accordingly
;	- On first start the script will kill all processes with the same names and start new fresh isntances to be monitored. 
;	- Next the script will determine if the machine is setup to display one or 2 Xibo instance and set configuration accordingly. 
;	- Once configuration is set the script will kill all processes with the same name/names to ensure no duplicate processes are running 
;	- Next it checks to see if the specified app/apps is running. 
;		- If the app/apps are not running and exists it wills tart it up
;		- If the app/apps are not running and can't be found a warning message will be shown
;	- If the app/apps are running it makes sure that the app/apps stay the active windows 
;	- On every hour the monitored instances will be restarted. 
;	- This script is then looped every 2 seconds, starting at the kill duplicates step. 
;  	- The short cut to pause this script is Windows+1. Press Windows+1 again to resume the script

;################ AutoHotkey settings ################
#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
#Persistent    ; keeps script running 
SetTimer Watchdog, 30000 ,10  ; start watchdogtimer 
OnExit, ExitSub

;################ Configuration Settings  ##############
FormatTime, FileTime,, MM/dd/yy hh:mm:ss tt
FormatTime, FileDate, ddmmyyyy, shortdate

StringReplace, FileDate, FileDate, /, , All

;################ Set Global Varbiales  ##############

;; Log File Variables 
InfoLogFile = %A_ScriptDir%\Logs\%FileDate%XiboOnTop.log
ErrorLogFile = %A_ScriptDir%\Logs\%FileDate%XiboOnTopError.log
traceConst = trace
infoConst = info
warningConst = warning
errorConst = error
Debug := false
Logging := false
chkInterval = 2000

;################ Create Log Folders  ##############
IfExist, %A_ScriptDir%\Logs\
{
	sleep 1
}
Else
{
	FileCreateDir, %A_ScriptDir%\Logs\
}


log("info","================ Starting XiboOnTop  ================  ")

;log("trace","This is a TRACE log message")
;log("info","This is a INFO log message")
;log("warning","This is a WARNING log message")
;log("error","This is a ERROR log message")


;; Set Working directory 
;WorkingDir = C:\Users\SPECTRE\Desktop\TestEnv\	; For Test purposes 
WorkingDir =  C:\

log("trace","Global variables set.")

;################ Specify Process to be monitored  ##############
osBitVersion := checkOsBitVersion()
logmessage =  OS Bit version set to %osBitVersion%bit
log("info",logmessage)

xiboInstances := checkXiboInstances(osBitVersion)
logmessage =  Found %xiboInstances% Xibo instances 
log("info",logmessage)

;################ Initialize Global process Query ###############
ProcessNameQuery := "Select * from Win32_Process  where name = '" ProcessNameString "'"



if(xiboInstances = 1)
{
	Gosub, freshStart1
	SetTimer xiboInstanceRun1, %chkInterval%, 0
	log("info","xiboIntanceRun1 Timer set.")

}
Else
{
	Gosub, freshStart2
	SetTimer xiboInstanceRun2, %chkInterval%, 0
	
	log("info","xiboIntanceRun2 Timer set.")

}
return




checkOsBitVersion()
{
	log("trace","Checking OS bit version....")
	global WorkingDir

	IfExist, %WorkingDir%Program Files (x86)\
		{
			osBitVersion := 64
		}
		else
		{
			osBitVersion := 32	
		}
		return osBitVersion
}		
	
checkXiboInstances(osBitVersion)
{
	log("trace","Checking number of Xibo instances....")
	global WorkingDir
	global ProcessName
	global ProcessNameString
	global ProcessPath
	global ProcessName1
	global ProcessNameString1
	global ProcessPath1
	global ProcessName2
	global ProcessNameString2
	global ProcessPath2

	If (osBitVersion = 32)
		{
			
			IfExist, %WorkingDir%Program Files\Xibo Player\
			{
				xiboInstances := 1
				ProcessName = XiboClient.exe
				ProcessNameString := "XiboClient.exe"
				ProcessPath = %WorkingDir%Program Files\Xibo Player\XiboClient.exe
				return xiboInstances
			}
			else
			{
				xiboInstances := 2
				ProcessName1 = XiboClient1.exe
				ProcessNameString1 := "XiboClient1.exe"
				ProcessPath1 = %WorkingDir%Program Files\Xibo Player1\XiboClient1.exe
				ProcessName2 = XiboClient2.exe
				ProcessNameString2 := "XiboClient2.exe"
				ProcessPath2 = %WorkingDir%Program Files\Xibo Player2\XiboClient2.exe
				return xiboInstances
				
			}
		}
	Else
		{
			
			IfExist, %WorkingDir%Program Files (x86)\Xibo Player\
			{
				xiboInstances := 1
				ProcessName = XiboClient.exe
				ProcessNameString := "XiboClient.exe"
				ProcessPath = %WorkingDir%Program Files (x86)\Xibo Player\XiboClient.exe
				return xiboInstances
			}
			else
			{
				xiboInstances := 2
				ProcessName1 = XiboClient1.exe
				ProcessNameString1 := "XiboClient1.exe"
				ProcessPath1 = %WorkingDir%Program Files (x86)\Xibo Player1\XiboClient1.exe
				ProcessName2 = XiboClient2.exe
				ProcessNameString2 := "XiboClient2.exe"
				ProcessPath2 = %WorkingDir%Program Files (x86)\Xibo Player2\XiboClient2.exe
				return xiboInstances
				
			}
		}
		;MsgBox %osBitVersion% - %xiboInstances%
		
}

xiboInstanceRun1:
		log("trace","Entering xiboInstanceRun1...")
		SetTimer xiboInstanceRun1, off
		log("trace","xiboInstanceRun1 timer stopped.")
		
		Gosub, SetWDTimer
		Gosub, killDuplicates1
		log("trace","Check to see if Process is running and active... ")
		Process, Exist, %ProcessName% 
		{	
			If (ErrorLevel = 0) 								; If it is not running
				{
					Gosub startApp1
				}
			else if (A_Min = 00) 						; is time x:00 ?
			{
				Gosub restartApp1
			}
			else										
			{
				Gosub makeActive1

			}
			
		}
		SetTimer xiboInstanceRun1, on
		log("trace","xiboInstanceRun1 timer turned back on.")
		return

xiboInstanceRun2:
		log("trace","Entering xiboInstanceRun2...")
		SetTimer xiboInstanceRun2, off
		log("trace","xiboInstanceRun2 timer stopped.")
		
		Gosub, SetWDTimer
		Gosub, killDuplicates2
		log("trace","Check to see if Processes are running and active:")
		
		Process, Exist, %ProcessName1%
		{
			ErrorLevel1 = %ErrorLevel%
		}
		
		Process, Exist, %ProcessName2%
		{
			ErrorLevel2 = %ErrorLevel%
		}

		If (ErrorLevel1 = 0) || (ErrorLevel2 =0 )
		{
			if (ErrorLevel2 = 0)
			{
				Gosub startApp22
			}
			if (ErrorLevel1 = 0)
			{
				Gosub startApp21
			}
		}
		else if (A_Min = 00) 						; is time x:00 ?
		{
				Gosub restartApp2
		}
		else										
		{
				Gosub makeActive2															
		}
		

		SetTimer xiboInstanceRun2, on
		log("trace","xiboInstanceRun2 timer turned back on.")
		return

freshStart1:
	log("trace","Starting fresh new instance....")
	count=0					
	for process in ComObjGet("winmgmts:").ExecQuery("Select * from Win32_Process")
	{
		if	(process.name = ProcessNameString )
			count++
	}	

	if count > 0
	{
		log("trace","Killing duplicates....")
		for process in ComObjGet("winmgmts:").ExecQuery("Select * from Win32_Process  where name = '" ProcessNameString "'")
		{
			process, close, % process.ProcessId	
		}
		
		sleep 1000
		
		
	}

	log("trace","Starting Process... ")
	IfExist, %ProcessPath%							; Check if App exists in path specified
		{
			
			Run,%ProcessPath%, , , ActivePID			;Start app from specified path and record PID

			logmessage =  Fresh Instance started with PID %ActivePID%.
			log("info",logmessage)
		}
		else
		{
			log("error","Could not find app in specified path!")
			MsgBox Program does not exist `nLooking for : %ProcessPath%	 `nExiting App		;Raise error if app can't be found
			log("info","Exiting App...")
			ExitApp

	}
	return

freshStart2:
	log("trace","Starting fresh new instance for app nr 2....")
	count=0					
	for process in ComObjGet("winmgmts:").ExecQuery("Select * from Win32_Process")
	{
		if	(process.name = ProcessNameString2 )
			count++
	}	

	if count > 0
	{
		log("trace","Killing duplicates....")
		for process in ComObjGet("winmgmts:").ExecQuery("Select * from Win32_Process  where name = '" ProcessNameString2 "'")
		{
			process, close, % process.ProcessId	
		}
		
		sleep 1000
		
		
	}
	log("trace","Starting Process 2... ")
	IfExist, %ProcessPath2%							; Check if App exists in path specified
		{
			Run,%ProcessPath2%, , , ActivePID2			;Start app from specified path and record PID
			logmessage =  Fresh Instance started with PID %ActivePID2%.
			log("info",logmessage)
			
		}
		else
		{
			log("error","Could not find app nr 2 in specified path!")
			MsgBox Program does not exist `nLooking for : %ProcessPath2%	 `nExiting App		;Raise error if app can't be found
			log("info","Exiting App...")
			ExitApp

	}

	log("trace","Starting fresh new instance for app nr 1....")
	count=0					
	for process in ComObjGet("winmgmts:").ExecQuery("Select * from Win32_Process")
	{
		if	(process.name = ProcessNameString1 )
			count++
	}	

	if count > 0
	{
		log("trace","Killing duplicates....")
		for process in ComObjGet("winmgmts:").ExecQuery("Select * from Win32_Process  where name = '" ProcessNameString1 "'")
		{
			process, close, % process.ProcessId	
		}
		
		sleep 1000
		
		
	}
	log("trace","Starting Process 1... ")
	IfExist, %ProcessPath1%							; Check if App exists in path specified
		{
			Run,%ProcessPath1%, , , ActivePID1			;Start app from specified path and record PID
			logmessage =  Fresh Instance started with PID %ActivePID1%.
			log("info",logmessage)
			
		}
		else
		{
			log("error","Could not find app nr 1 in specified path!")
			MsgBox Program does not exist `nLooking for : %ProcessPath1%	 `nExiting App		;Raise error if app can't be found
			log("trace","Killing Process 2...")
			process, close, %ActivePID2%
			log("info","Exiting App...")
			ExitApp

	}
	return

killDuplicates1:
	log("trace","Killing duplicate processes....")
	count=0	
	log("trace","Looking for duplciate processes....")				
	for process in ComObjGet("winmgmts:").ExecQuery("Select * from Win32_Process")
	{
		if	(process.name = ProcessNameString )
			count++
	}	
		
	if (count > 0)
	{
		ActiveProcessID := ActivePID 
		for process in ComObjGet("winmgmts:").ExecQuery("Select * from Win32_Process  where name = '" ProcessNameString "'")
		{
			processID := process.ProcessId
			if (processID != ActiveProcessID)
				{

					process, close, % process.ProcessId	
					logmessage =  Killed duplicate process with PID %processID%.
					log("warning",logmessage)	
				}
		}		
	}
	
	return

killDuplicates2:
	log("trace","Killing duplicate processes for app nr 1....")
	count=0					
	log("trace","Looking for duplciate processes....")
	for process in ComObjGet("winmgmts:").ExecQuery("Select * from Win32_Process")
	{
		if	(process.name = ProcessNameString1 )
			count++
	}	
		
	if (count > 0)
	{
		ActiveProcessID := ActivePID1 
		for process in ComObjGet("winmgmts:").ExecQuery("Select * from Win32_Process  where name = '" ProcessNameString1 "'")
		{
			processID := process.ProcessId
			if (processID != ActiveProcessID)
				{
					process, close, % process.ProcessId		
					logmessage =  Killed duplicate process with PID %processID%.
					log("warning",logmessage)	
				}
		}		
	}
	log("trace","Killing duplicate processes for app nr 2....")
	count=0		
	log("trace","Looking for duplciate processes....")			
	for process in ComObjGet("winmgmts:").ExecQuery("Select * from Win32_Process")
	{
		if	(process.name = ProcessNameString2 )
			count++
	}	
		
	if (count > 0)
	{
		ActiveProcessID := ActivePID2
		for process in ComObjGet("winmgmts:").ExecQuery("Select * from Win32_Process  where name = '" ProcessNameString2 "'")
		{
			processID := process.ProcessId
			if (processID != ActiveProcessID)
				{
					process, close, % process.ProcessId		
					logmessage =  Killed duplicate process with PID %processID%.
					log("warning",logmessage)	
				}
		}		
	}
	return

startApp1:
	IfExist, %ProcessPath%							; Check if App exists in path specified
		{
			Run,%ProcessPath%, , , ActivePID			;Start app from specified path and record PID

			logmessage =  Process was not running, restarted process with PID %ActivePID%.
			log("warning",logmessage)
			
		}
		else
		{
			log("error","Could not find app in specified path!")
			MsgBox Program does not exist `nLooking for : %ProcessPath%	 `nExiting App		;Raise error if app can't be found
			log("info","Exiting App...")
			ExitApp

	}
	return

startApp21:
	IfExist, %ProcessPath1%							; Check if App exists in path specified
		{
			Run,%ProcessPath1%, , , ActivePID1			;Start app from specified path and record PID
			logmessage =  Process 1 was not running, restarted process with PID %ActivePID1%.
			log("warning",logmessage)
			
		}
		else
		{	
			log("error","Could not find app nr 1 in specified path!")
			MsgBox Program does not exist `nLooking for : %ProcessPath1%	 `nExiting App		;Raise error if app can't be found
			log("trace","Killing Process 2...")
			process, close, %ActivePID2%
			log("info","Exiting App...")
			ExitApp

	}
	return

startApp22:
	IfExist, %ProcessPath2%							; Check if App exists in path specified
		{
			Run,%ProcessPath2%, , , ActivePID2			;Start app from specified path and record PID
			logmessage =  Process 2 was not running, restarted process with PID %ActivePID2%.
			log("warning",logmessage)
			
		}
		else
		{
			log("error","Could not find app nr 2 in specified path!")
			MsgBox Program does not exist `nLooking for : %ProcessPath2%	 `nExiting App		;Raise error if app can't be found
			log("trace","Killing Process 1...")
			process, close, %ActivePID1%
			log("info","Exiting App...")
			ExitApp

	}
	return

restartApp1:
	{
		log("trace","Ontop of the hour, need to restart the Process.")
		log("trace","Killing process...")
		process, close, %ActivePID%
		sleep 1000
		
		;/*------- Check to see if all processes are stopped

		count=0					
		log("trace","Check to see if all processes are stopped...")
		for process in ComObjGet("winmgmts:").ExecQuery("Select * from Win32_Process")
		{
			if	(process.name = ProcessNameString )
				count++
		}	
					
		if count > 0
		{
			log("warning","All processes did not stop, killing them now...")
			for process in ComObjGet("winmgmts:").ExecQuery("Select * from Win32_Process  where name = '" ProcessNameString "'")
			{
				process, close, % process.ProcessId	
			}
			
			sleep 1000
			
			IfExist, %ProcessPath%							; Check if App exists in path specified
			{
				Run,%ProcessPath%, , , ActivePID			;Start app from specified path and record PID
				logmessage =  Restarted process with PID %ActivePID%.
				log("info",logmessage)
				
				
			}
			else
			{
				log("error","Could not find app in specified path!")
				MsgBox Program does not exist `nLooking for : %ProcessPath%			;Raise error if app can't be found
				log("info","Exiting App...")
				Exitapp
			}
		}
		else
		{
			IfExist, %ProcessPath%							; Check if App exists in path specified
			{
				Run,%ProcessPath%, , , ActivePID			;Start app from specified path and record PID
				logmessage =  Restarted process with PID %ActivePID%.
				log("info",logmessage)
				
			}
			else
			{
				log("error","Could not find app in specified path!")
				MsgBox Program does not exist `nLooking for : %ProcessPath%			;Raise error if app can't be found
				log("info","Exiting App...")
				Exitapp
			}
		}
		log("trace","Sleeping for a minute")
		sleep 60000	
	}
	return

restartApp2:
	{
		log("trace","Ontop of the hour, need to restart the Processes.")
		log("trace","Killing processes...")
		process, close, %ActivePID1%
		process, close, %ActivePID2%
		sleep 1000
		
		;/*------- Check to see if all processes are stopped
		
		log("trace","Check to see if all processes are stopped from process 2...")
		count=0					
		for process in ComObjGet("winmgmts:").ExecQuery("Select * from Win32_Process")
		{
			if	(process.name = ProcessNameString2 )
				count++
		}	
					
		if count > 0
		{
			log("warning","All processes did not stop, killing them now...")
			for process in ComObjGet("winmgmts:").ExecQuery("Select * from Win32_Process  where name = '" ProcessNameString2 "'")
			{
				process, close, % process.ProcessId	
			}
			
			sleep 1000
			
			IfExist, %ProcessPath2%							; Check if App exists in path specified
			{
				Run,%ProcessPath2%, , , ActivePID2			;Start app from specified path and record PID
				logmessage =  Restarted process 2 with PID %ActivePID2%.
				log("info",logmessage)
				
			}
			else
			{
				log("error","Could not find app nr 2 in specified path!")
				MsgBox Program does not exist `nLooking for : %ProcessPath2%			;Raise error if app can't be found
				log("info","Exiting App...")
				Exitapp
			}
		}
		else
		{
			IfExist, %ProcessPath2%							; Check if App exists in path specified
			{
				Run,%ProcessPath2%, , , ActivePID2			;Start app from specified path and record PID
				logmessage =  Restarted process 2 with PID %ActivePID2%.
				log("info",logmessage)
				
			}
			else
			{
				log("error","Could not find app nr 2 in specified path!")
				MsgBox Program does not exist `nLooking for : %ProcessPath2%			;Raise error if app can't be found
				log("info","Exiting App...")		
				Exitapp
			}
		}
		log("trace","Check to see if all processes are stopped from process 1...")
		count=0					
		for process in ComObjGet("winmgmts:").ExecQuery("Select * from Win32_Process")
		{
			if	(process.name = ProcessNameString1 )
				count++
		}	
					
		if count > 0
		{
			log("warning","All processes did not stop, killing them now...")
			for process in ComObjGet("winmgmts:").ExecQuery("Select * from Win32_Process  where name = '" ProcessNameString1 "'")
			{
				process, close, % process.ProcessId	
			}
			
			sleep 1000
			
			IfExist, %ProcessPath1%							; Check if App exists in path specified
			{
				Run,%ProcessPath1%, , , ActivePID1			;Start app from specified path and record PID
				logmessage =  Restarted process 1 with PID %ActivePID1%.
				log("info",logmessage)
				
			}
			else
			{
				MsgBox Program does not exist `nLooking for : %ProcessPath1%			;Raise error if app can't be found
				Exitapp
			}
		}
		else
		{
			IfExist, %ProcessPath1%							; Check if App exists in path specified
			{
				Run,%ProcessPath1%, , , ActivePID1			;Start app from specified path and record PID
				logmessage =  Restarted process 1 with PID %ActivePID1%.
				log("info",logmessage)
				
			}
			else
			{
				log("error","Could not find app nr 2 in specified path!")
				MsgBox Program does not exist `nLooking for : %ProcessPath1%			;Raise error if app can't be found
				log("trace","Killing Process 2...")
				process, close, %ActivePID2%
				log("info","Exiting App...")
				Exitapp
			}
		}
		log("trace","Sleeping for a minute")
		sleep 60000	
	}
	return

makeActive1:
	log("trace","Check to see if the process is the active window....")
	IfWinActive, ahk_pid %ActivePID% 					;If App is active do nothing
	{				
		log("trace","Process was active, not taking action")
		sleep 1000	
	}
	else
	{
		log("warning","Process not active, making it active.... ")
		WinActivate, ahk_pid %ActivePID%										;Make app active if it is not 

	}	
	return
		
makeActive2:
	
	Screen1 := false
	Screen2 := false
	log("trace","Checking to see if the process 1 is the active window....")
	
	ifWinActive, ahk_pid %ActivePID1% 											;If App is active do nothing	
	{				
		Screen1 := true	
	}

	WinGet, hwnd, ID, ahk_pid %ActivePID1% 
	
	log("trace","Checking to see if the process 2 is the second active window....")
	LastActivePID := findNextWindow(hwnd)
	;LastActivePID := findNextWindow()
	log("trace","Checking to see if the next active window is process 2...")
	if (ActivePID2 = LastActivePID)
	{
		Screen2 := true	
	}
	
	if (Screen1 && Screen2)
	{
		log("trace","Both Processes were active, not taking action")
		sleep 1000
	}
	else
	{	
		log("warning","Processes were not active, making them active.... ")
		WinActivate, ahk_pid %ActivePID2%
		WinActivate, ahk_pid %ActivePID1%
	}
	return

findNextWindow(hwnd)
{
	
	;hwnd := WinActive("A")

	Loop
	{
		log("trace","Getting the next active window...")
		hwnd := DllCall("GetWindow",uint,hwnd,uint,2) 		; 2 = GW_HWNDNEXT
		;SetFormat,integer,hex
		hwnd += 0
		;SetFormat,integer,d

		log("trace","Check to see if the next window is visible...")
		; GetWindow() processes even hidden windows, so we move down the z oder until the next visible window is found
		if (DllCall("IsWindowVisible",uint,hwnd) = 1)
			break
		if(A_LastError != 0)
		{
			if(A_LastError = 183)
			{
				sleep 1
			}
			Else
			{
				logmessage =  Error on finding next active window with error code: %A_LastError%
				log("warning",logmessage)
				return 0
			}			
		}
		sleep 200
	}
	WinGet, LastActivePID, PID, ahk_id %hwnd%
	Return, %LastActivePID%
}


SetWDTimer:
	SetTimer Watchdog,Off
	log("trace","Wathdog Timer stopped.")
	Timer := 6000000
	SetTimer Watchdog,%Timer%
	log("trace","Wathdog Timer reset.")
	SetTimer Watchdog,On
	log("trace","Wathdog Timer turned back on.")
	return

Watchdog:
	If (xiboInstances = 1)
	{
		log("error","Wathdog Timer was not reset, reloading script.")
		process, close, %ActivePID%
		log("info","Process closed.")
		Sleep 5000
		Reload
		return
	}
	else if  (xiboInstances = 2)
	{
		log("error","Wathdog Timer was not reset, reloading script.")
		process, close, %ActivePID%
		process, close, %ActivePID1%
		log("info","Process 1 closed.")
		process, close, %ActivePID2%
		log("info","Process 2 closed.")
		Sleep 5000
		Reload		
		return

	}
	else
	{
		log("error","Wathdog Timer was not reset, reloading script.")
		Sleep 5000
		Reload		
		return
	}
	


Log(logtype,logmessage)
{
	global InfoLogFile
	global ErrorLogFile
	
	global FileDate
	global Debug
	global Logging

	global traceConst
	global infoConst
	global warningConst
	global errorConst

	FormatTime, FileTime,, MM/dd/yy hh:mm:ss tt

	if (Logging)
	{
		if traceConst  = %logtype%
		{
			if(Debug)
			{
				prefix = TRACE:		:
				TraceLogMessage = %prefix% %FileTime% - %logmessage%`n
				fileappend, %TracelogMessage%, %InfoLogFile%
			}
		}

		if infoConst  = %logtype%
		{
			prefix = INFO:		:
			InfoLogMessage = %prefix% %FileTime% - %logmessage%`n
			fileappend, %InfologMessage%, %InfoLogFile%
		}

		if warningConst  = %logtype%
		{
			prefix = WARNING:	:
			WarningLogMessage = %prefix% %FileTime% - %logmessage%`n
			fileappend, %WarninglogMessage%, %InfoLogFile%
		}

		
	}
	if errorConst = %logtype%
	{
		prefix = ERROR:		:
		ErrorLogMessage = %prefix% %FileTime% - %logmessage%`n
		fileappend, %ErrorlogMessage%, %ErrorLogFile%
		if (Logging)
		{
			fileappend, %ErrorlogMessage%, %InfoLogFile%
		}
	}
	

	return
}


ExitSub:
	log("info","================ App Closed ================  ")	
ExitApp

;#q::ExitApp
#1::Pause  	
