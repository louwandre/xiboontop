# README #



### What is this repository for? ###

This script Monitors 1 to 2 XiboClient instances.
It detects whether the app is running, starts it up if it isn't and makes sure it stays the active window. 
* Version 0.3


### How do I get set up? ###

* Install AutoHotKey and compile source code (.ahk) using AutoHotKey. 
* Configuration: No configuration required - Only monitors XiboClient.exe
* Dependencies: Windows 7/8.1, XiboClient.exe
* Deployment instructions: Ensure Xibo Client is installed according to the APC documentation and deploy xiboOnTop app to any folder on the machine.  

### Contribution guidelines ###

Get in touch with Owner

### Who do I talk to? ###

Andre Louw <alouw@apcglobal.com>